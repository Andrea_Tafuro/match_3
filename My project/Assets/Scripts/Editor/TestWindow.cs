using UnityEngine;
using UnityEditor;

public class TestWindow : EditorWindow
{
    private string myString;

    public GameObject[] prefab;
    SerializedObject so;

    [MenuItem("Window/Custom/Esempio")]

    
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<TestWindow>("Esempio Nome Della Finestra");
    }

    private void OnEnable()
    {
        ScriptableObject target = this;
        so = new SerializedObject(target);
    }

    private void OnGUI()
    {
        /*
        GUILayout.Label("My label", EditorStyles.boldLabel);

        myString =  EditorGUILayout.TextField("Name", myString);

        if (GUILayout.Button("Press Me"))
        {
            Debug.Log(myString);
        }
        */

        so.Update();

        SerializedProperty objectToDysplay = so.FindProperty("prefabs");
        EditorGUILayout.PropertyField(objectToDysplay, true);
        so.ApplyModifiedProperties();

        GUILayout.Label("Spawn Random Item", EditorStyles.boldLabel);

        if(GUILayout.Button("Spawn Item"))
        {
            int index = Random.Range(0, prefab.Length);
            Instantiate(prefab[index], Random.insideUnitSphere * 10, Quaternion.identity);

        }

        GUILayout.Label("Delete Selected Item", EditorStyles.boldLabel);
        if(GUILayout.Button("Delete"))
        {
            foreach(GameObject obj in Selection.gameObjects)
            {
                DestroyImmediate(obj);
            }
        }
    }
}
