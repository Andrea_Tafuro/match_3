using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class GameEngine : MonoBehaviour
{
    [SerializeField]
    public GameObject contenitoreGriglia;

    [SerializeField]
    public int nRighe, nColonne;
    [SerializeField]
    float lunghezzaDisplay = 100;
    [SerializeField]
    float larghezzaDisplay = 180;

    float distanzaTraRighe, distanzaTraColonne;

    Oggetto[,] grigliaOggetti;

    [SerializeField]
    public GameObject oggettoDaSpawnare;

    [SerializeField]
    public GameObject[] listaOggetti = new GameObject[4];

    [SerializeField]
    public  GameObject testoPunteggio;

    public int punteggio;

    public GameObject oggettoToccato;
    Vector3 posizioneOggettoPartenza;
    Ray rilascioTocco;


    void Start()
    {
        testoPunteggio.GetComponent<Text>().text = "0";
        Griglia();
    }

    void Griglia()
    {
        grigliaOggetti = new Oggetto[nRighe, nColonne];

        distanzaTraColonne = lunghezzaDisplay / nColonne;
        distanzaTraRighe = larghezzaDisplay / (nRighe + 3);

        for (int a = 0; a < nRighe; a = a + 1)
        {
            for (int b = 0; b < nColonne; b = b + 1)
            {
                RandomOggetti();

                grigliaOggetti[a, b].Posizione = new Vector3(distanzaTraRighe * b, distanzaTraColonne * a, 0)
                    + contenitoreGriglia.transform.position;

                grigliaOggetti[a, b].Tipo = oggettoDaSpawnare;

                grigliaOggetti[a, b].Ref = Instantiate(oggettoDaSpawnare, grigliaOggetti[a, b].Posizione,
                                                        Quaternion.identity, contenitoreGriglia.transform);

                grigliaOggetti[a, b].Ref.GetComponent<datiOggetto>().x = a;
                grigliaOggetti[a, b].Ref.GetComponent<datiOggetto>().y = b;
            }
        }
        //ControlloTris();
    }

    void RandomOggetti()
    {
        int x = Random.Range(0, 4);
        oggettoDaSpawnare = listaOggetti[x];
    }

    void ControlloTris()
    {

        GameObject x;
        GameObject y;
        GameObject z;

        //Controllo verticale
        for (int b = 0; b < nColonne; b++)
        {
            for (int a = 0; a < nRighe; a++)
            {

                x = grigliaOggetti[a, b].Tipo;

                if (a + 2 < nRighe)
                {
                    y = grigliaOggetti[a + 1, b].Tipo;

                    if (x == y)
                    {
                        z = grigliaOggetti[a + 2, b].Tipo;

                        if (z == y)
                        {
                            Destroy(grigliaOggetti[a, b].Ref);
                            Destroy(grigliaOggetti[a + 1, b].Ref);
                            Destroy(grigliaOggetti[a + 2, b].Ref);

                            punteggio += 100;
                        }
                    }
                }
            }
        }
        //Controllo orizzontale
        for (int a = 0; a < nRighe; a++)
        {
            for (int b = 0; b < nColonne; b++)
            {

                x = grigliaOggetti[a, b].Tipo;

                if (b + 2 < nColonne)
                {
                    y = grigliaOggetti[a, b + 1].Tipo;

                    if (x == y)
                    {
                        z = grigliaOggetti[a, b + 2].Tipo;

                        if (z == y)
                        {
                            Destroy(grigliaOggetti[a, b].Ref);
                            Destroy(grigliaOggetti[a, b + 1].Ref);
                            Destroy(grigliaOggetti[a, b + 2].Ref);

                            punteggio += 100;
                        }
                    }
                }
            }
        }
    }

    void Rimpiazzo()
    {
        for (int b = 0; b < nColonne; b++)
        {
            for (int a = 0; a < nRighe; a++)
            {
                if (grigliaOggetti[a, b].Ref == null)
                {
                    RandomOggetti();

                    grigliaOggetti[a, b].Tipo = oggettoDaSpawnare;

                    grigliaOggetti[a, b].Ref = Instantiate(oggettoDaSpawnare, grigliaOggetti[a, b].Posizione,
                                                        Quaternion.identity, contenitoreGriglia.transform);

                    grigliaOggetti[a, b].Ref.GetComponent<datiOggetto>().x = a;
                    grigliaOggetti[a, b].Ref.GetComponent<datiOggetto>().y = b;

                }
            }
        }
    }
    private void FixedUpdate()
    {
        ControlloTris();
        Rimpiazzo();
        testoPunteggio.GetComponent<Text>().text = punteggio.ToString();
    }

    void Update()
    {

        if (Input.touchCount > 0)
        {
            rilascioTocco = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (oggettoToccato == null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;


                if (Physics.Raycast(ray, out hit))
                {
                    oggettoToccato = hit.transform.gameObject;
                    posizioneOggettoPartenza = oggettoToccato.transform.position;

                }
                else
                {
                    Debug.DrawRay(ray.origin, ray.direction, Color.yellow);
                    Debug.Log("Mancato");
                }
            }
            else
            {
                oggettoToccato.transform.position = Camera.main.ScreenPointToRay(Input.GetTouch(0).position).origin;
            }
        }
        else if (oggettoToccato != null)
        {
            RaycastHit hit;

            BoxCollider[] collider = oggettoToccato.GetComponents<BoxCollider>();
            collider[0].enabled = false;

                int a = oggettoToccato.GetComponent<datiOggetto>().x;
                int b = oggettoToccato.GetComponent<datiOggetto>().y;

            if (Physics.Raycast(rilascioTocco, out hit))
            {
                collider[0].enabled = true;

                int j = hit.transform.gameObject.GetComponent<datiOggetto>().x;
                int k = hit.transform.gameObject.GetComponent<datiOggetto>().y;

                Oggetto Oggetto1 = grigliaOggetti[j, k];
                grigliaOggetti[j, k] = grigliaOggetti[a, b];
                grigliaOggetti[a, b] = Oggetto1;

                oggettoToccato = null;

                ControlloTris();
                Rimpiazzo();
                testoPunteggio.GetComponent<Text>().text = punteggio.ToString();
            }
            else
            {
                collider[0].enabled = true;
                oggettoToccato.transform.position = posizioneOggettoPartenza;
                oggettoToccato = null;
            }
        }

    }

    public struct Oggetto
    {
        public Vector3 Posizione;
        public GameObject Tipo;
        public GameObject Ref;
        public int x;
        public int y;
    }
}
